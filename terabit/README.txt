INTRODUCTION
------------
Re-Designed and developed by: Christian McQuilkin
Feel free to email me if you have any questions at:
chris.e.mcquilkin@gmail.com

CONFIGURATION
-------------
To edit the footer of the theme head to templates/page.html.twig and it will be
on the very bottom. Other than that it has the basic function of Seven There
will be more functions later on (can be requested), but we will have to wait 
until Drupal 8 is fully released.
